<?php

class MyParentClass{
    public $imAPublicProperty = "Public Property";
    protected $imAProtectedProperty = "Protected Property";
    private $imAPrivateProperty = "Private Property";

    private $helloWorld = "Hello";

    public function setHelloWorld($helloWorld)
    {
        $this->helloWorld = $helloWorld;
    }

    public function getHelloWorld()
    {
        return $this->helloWorld;
    }

    public function setImAPrivateProperty($imAPrivateProperty)
    {
        $this->imAPrivateProperty = $imAPrivateProperty;
    }

    public function getImAPrivateProperty()
    {
        return $this->imAPrivateProperty;
    }

    public function doSomethingPublic(){
        echo "Now I'm inside the ".__METHOD__."<br>";
        $this->doSomethingPrivate();
    }
    protected function doSomethingProtected(){
        echo "Now I'm inside the ".__METHOD__."<br>";
    }
    private function doSomethingPrivate(){
        "Now I'm inside the ".__METHOD__."<br>";
    }
}

$object_OfMyParentClassGolpahar = new MyParentClass();
$object1_OfMyParentClassAgrabad = new MyParentClass();
$object2_OfMyParentClassChawkbazar = new MyParentClass();


$object_OfMyParentClassGolpahar->imAPublicProperty = "Public Property Mehedi";
echo $object_OfMyParentClassGolpahar->imAPublicProperty."<br>";

$object_OfMyParentClassGolpahar->setImAPrivateProperty("Private Property Hasan");
echo $object_OfMyParentClassGolpahar->getImAPrivateProperty()."<br>";

$object_OfMyParentClassGolpahar->doSomethingPublic();

echo $object_OfMyParentClassGolpahar->getHelloWorld()."<br>";

$object_OfMyParentClassGolpahar->setHelloWorld($object_OfMyParentClassGolpahar->getHelloWorld() ."World");
echo $object_OfMyParentClassGolpahar->getHelloWorld()."<br>";



echo str_repeat("<br>",15);

////end of my parent class/////


/*class MyChildClass extends MyParentClass
......................... video theke dekhe nite hobe. inheritence ekhan theke suru hobe.
    */